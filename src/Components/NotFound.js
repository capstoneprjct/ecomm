import React from 'react'
import { NavLink } from 'react-router-dom'

function NotFound() {
    return (
        <>
            <div className='bg-dark fixed-top' style={{ width: "auto", height: "98px" }}>
                <div className='col'>
                    <NavLink to='/'><img src='\images\clockit.png' alt='ClockIT' style={{ width: '66px', height: '66px', borderRadius: '22px', float: 'left', margin: '5px' }} /></NavLink>
                    <p className='text-light fs-1 m-2 p-2' style={{ fontFamily: 'fantasy', float: 'left' }}>ClockIT</p>
                </div>
            </div>
            <div className='border m-5 fs-3 d-flex justify-content-center'>
                Page Not Found <br />
                Recheck the URL
            </div>
        </>
    )
}

export default NotFound