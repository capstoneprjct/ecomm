export const addCart = (product) => {
    return {
        type: "ADD_ITEM",
        data: product
    }
}
export const delCart = (product) => {
    return {
        type: "DEL_ITEM",
        data: product
    }
}
