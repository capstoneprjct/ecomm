import rootReducers from "./reducer";
import { legacy_createStore } from 'redux';

const store = legacy_createStore(rootReducers);

export default store