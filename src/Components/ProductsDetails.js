import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import * as i from 'react-bootstrap-icons'
import { useDispatch } from 'react-redux';
import { addCart } from './redux/action';

function ProductsDetails() {
    const { id } = useParams()
    const [data, setData] = useState([])

    const dispatch = useDispatch();
    const addProduct = (product) => {
        dispatch(addCart(product));
    }


    useEffect(() => {
        axios.get(`http://localhost:4000/products/${id}`)
            .then(resp => {
                setData(resp.data)
                console.log(resp.data)
            })
    }, [])

    // useEffect(() => {
    //     const get = async () => {
    //         const resp = await fetch(` http://localhost:4000/products/${id}`);
    //         setData(await resp.json());
    //         console.log(resp);
    //     };
    //     get();
    // }, []);


    return (
        <>
            <div className='container m-4'>
                <div className='row'>
                    <div className='col'>
                        <img src={data.img} class="card-img-top my-3 rounded-5" alt={data.id} style={{ width: "500px", height: "500px" }}></img>
                    </div>
                    <div className='col'>
                        <div className='text-danger fs-1 fw-bold'>INFO OF {data.title}</div>
                        <h4 class="text-success my-4 p-3 border border-secondary border-3" style={{ width: "260px" }}> Price:<i.CurrencyRupee />{data.price}/-</h4>

                        <div className='fs-5 my-4'>{data.des}</div>
                        <nav>
                            <Link to='/cart' class="btn btn-success mx-1" onClick={() => addProduct(data)}>Add to cart</Link>
                            <Link to='/product' class="btn btn-warning mx-1">Back to Products</Link>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductsDetails