import React, { useEffect, useState } from 'react'
import * as i from 'react-bootstrap-icons'
import { NavLink, Outlet, useNavigate } from 'react-router-dom'
import '../App.css'
import { toast } from 'react-toastify'

function Login() {
  const [name, setName] = useState('')
  const [pw, setPW] = useState('')

  const navigate = useNavigate()
  // useEffect = () => {
  //   sessionStorage.clear()
  // }

  const ProceedLogin = (e) => {
    e.preventDefault();
    if (validate()) {
      fetch("http://localhost:4000/users/" + name).then((res) => {
        return res.json();
      }).then((resp) => {

        if (Object.keys(resp).length === 0) {
          toast.error('Please Enter valid username');
        } else {
          if (resp.pw === pw) {
            toast.success('Successfully logged in');
            sessionStorage.setItem('username', name);
            sessionStorage.setItem('userrole', resp.role);
            navigate('/home')
          } else {
            toast.error('Please Enter valid credentials');
          }
        }
      }).catch((err) => {
        toast.error(err.message);
      });
    }
  }

  const validate = () => {
    let result = true
    if (name === '' || name === null) {
      result = false
      alert('Enter Name')
    }
    if (pw === '' || pw === null) {
      result = false
      alert('Enter password')
    }
    return result
  }

  return (
    <>
      <div className='bg-dark fixed-top' style={{ width: "auto", height: "98px" }}>
        <div className='col'>
          <NavLink to='/'><img src='\images\clockit.png' alt='ClockIT' style={{ width: '66px', height: '66px', borderRadius: '22px', float: 'left', margin: '5px' }} /></NavLink>
          <p className='text-light fs-1 m-2 p-2' style={{ fontFamily: 'fantasy', float: 'left' }}>ClockIT</p>
        </div>
      </div>
      <div className="py-4 my-3" style={{ backgroundColor: "lavender" }}>
        <div className='container-fluid text-center' style={{ marginLeft: "340px" }}>
          <div className='border border-dark border-3 my-3' style={{ width: '580px' }}>
            <div className='row'>

              <button type="button" class="btn bg-secondary-subtle my-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i.Google />Sign in with Google
              </button>

              <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h1 class="modal-title fs-5" id="exampleModalLabel">SIGN IN</h1>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form style={{ width: "auto" }}>
                        <div class="form-floating mb-3">
                          <input type="text" class="form-control" id="floatingInputid" placeholder="ID" />
                          <label for="floatingInput"><i.Person />ID</label>
                        </div>
                        <div class="form-floating">
                          <input type="password" class="form-control" id="floatingPassword" placeholder="Password" />
                          <label for="floatingPassword"> Password</label>
                        </div>
                        <NavLink to='/home' className='btn btn-warning my-5 mx-2'>Sign IN</NavLink>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

              <button type="button" class="btn bg-success-subtle my-2" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i.Facebook /> Sign in with Facebook
              </button>
              <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h1 class="modal-title fs-5" id="exampleModalLabel">SIGN IN</h1>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="form-floating mb-3">
                          <input type="text" class="form-control" id="floatingInput" placeholder="ID" />
                          <label for="floatingInput"><i.Person />ID</label>
                        </div>
                        <div class="form-floating">
                          <input type="password" class="form-control" id="floatingPassword" placeholder="Password" />
                          <label for="floatingPassword"> Password</label>
                        </div>
                        <NavLink to='/home' className='btn btn-warning my-2 mx-2'>Sign IN</NavLink>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>

              <p className='fw-bold fs-2'>------OR------</p>
              <form style={{ width: '560px' }} onSubmit={ProceedLogin}>
                <div class="form-floating mb-3">
                  <input type="text" value={name} onChange={(e) => setName(e.target.value)} class="form-control" id="floatingInput" placeholder="UserID" />
                  <label for="floatingInput"><i.Person /> UserID</label>
                </div>
                <div class="form-floating">
                  <input type="password" value={pw} onChange={(e) => setPW(e.target.value)} class="form-control" id="floatingPassword" placeholder="Password" />
                  <label for="floatingPassword"> Password</label>
                </div>

                <button className='btn btn-warning my-2 mx-2'>SIGN IN</button>
                <div> New user? <NavLink to="/register"> Register here</NavLink></div>
                <Outlet />
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Login

