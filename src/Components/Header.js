import React from 'react'
import { NavLink } from 'react-router-dom'
import * as i from 'react-bootstrap-icons'
import { useSelector } from 'react-redux'

function Header() {
  const state = useSelector((state) => state.handleCart)
  return (
    <>
      <div className='bg-dark' style={{ width: "auto", height: "98px" }}>
        <div className='col'>
          <NavLink to='/'><img src='\images\clockit.png' alt='ClockIT' style={{ width: '66px', height: '66px', borderRadius: '22px', float: 'left', margin: '5px' }} /></NavLink>
          <p className='text-light fs-1 m-2 p-2' style={{ fontFamily: 'fantasy', float: 'left' }}>ClockIT</p>
        </div>
<br/>
        <ul className=" button d-flex justify-content-end py-1 pe-3">
          <li className="nav-item fs-5 p-1 btn btn-outline-light ms-2">
            <NavLink className="nav-link" aria-current="page" to="/home">HOME</NavLink>
          </li>
          <li className="nav-item fs-5 p-1 btn btn-outline-light ms-2">
            <NavLink className="nav-link" to="/about">ABOUT</NavLink>
          </li>
          <li className="nav-item fs-5 p-1 btn btn-outline-light ms-2">
            <NavLink className="nav-link" to="/product">PRODUCT</NavLink>
          </li>
          <li className="nav-item fs-5 p-1 btn btn-outline-light ms-2">
            <NavLink className="nav-link" to="/employee">EMPLOYEE</NavLink>
          </li>
          <li className="nav-item fs-5 p-1 btn btn-outline-light ms-2">
            <NavLink className="nav-link" to="/cart" style={{ textDecoration: {} }}>CART<i.Cart2 className='ms-1' />({state.length})</NavLink>
          </li>
          <li className="nav-item fs-5 p-1 btn btn-outline-danger ms-2">
          <NavLink className='nav-link fst-italic' to='/'>LOGOUT</NavLink>
          </li>
        </ul>
      </div>
    </>
  )
}

export default Header