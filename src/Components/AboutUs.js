import React, { useEffect } from 'react'
import * as i from 'react-bootstrap-icons'
import { useNavigate } from 'react-router-dom';
function AboutUs() {
    // const navigate = useNavigate()
    // useEffect(() => {
    //     let name = sessionStorage.getItem('name');
    //     if (name === '' || name === null) {
    //         navigate('/')
    //     }
    // }, [])

    return (
        <div style={{ backgroundColor: "darkkhaki" }}>
            <h4 className='d-flex justify-content-center mt-3 pt-2 fw-bold fs-2' style={{ textShadow: "2px 1px purple" }}>GLIMPSE ABOUT US</h4>
            <div className='container border border-5 border-dark my-5 px-4 py-4 fs-4' style={{ width: '700px' }}>
                ClockIT is a one stop place for all watches and clocks with wide varieties involved. We have brands including Titian Raga, Wrogn, Tommy Hilfiger, Guess, French Connection to name a few.
                <br /><br />
                <pre>
                    Feel free to check us out on our social hands:<br />
                    <i.Facebook /> https:facebook.com\timezone.store<br />
                    <i.Instagram /> @TimeZone.store<br />
                    <i.Twitter /> @TimeZone<br /><br />
                    Or reach out for details on:<br />
                    <i.Whatsapp /> +91 1234567890<br />
                </pre>
            </div>
        </div>
    )
}

export default AboutUs