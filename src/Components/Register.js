import React, { useState } from 'react'
import { useForm } from 'react-hook-form'
import { Link, NavLink, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'

function Register() {
  const [id, setid] = useState('')
  const [name, setName] = useState('')
  const [phone, setPhone] = useState('')
  const [email, setEmail] = useState('')
  const [pw, setpw] = useState('')
  const [dob, setDOB] = useState('')
  const [address, setAddress] = useState('')
  const [plan, setPlan] = useState('')

  const navigate = useNavigate()

  const validate = () => {
    let isproceed = true
    let errmsg = 'Error at '
    if (name === null || name === '') {
      isproceed = false
      errmsg += ' Name'
    }
    if (phone === null || phone === '') {
      isproceed = false
      errmsg += ' Phone'
    }
    if (email === null || email === '') {
      isproceed = false
      errmsg += ' Email'
    }
    if (pw === null || pw === '') {
      isproceed = false
      errmsg += ' Password'
    }
    if (dob === null || dob === '') {
      isproceed = false
      errmsg += ' Date of Birth'
    }
    if (plan === null || plan === '') {
      isproceed = false
      errmsg += ' Opted plan '
    }
    if (!isproceed) {
      toast.warning(errmsg)
    }
    return isproceed

  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (validate()) {
      let data = { id, name, phone, email, pw, dob, address, plan }
      // console.log(data)

      fetch("http://localhost:4000/users", {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(data)
      }).then((res) => {
        toast.success('Registration completed')
        navigate("/")
      }).catch((err) => {
        toast.error('Failed, Please check again' + err.message)
      })
    }
  }
  return (
    <>
      <div className='bg-dark fixed-top' style={{ width: "auto", height: "98px" }}>
        <div className='col'>
          <NavLink to='/'><img src='\images\clockit.png' alt='ClockIT' style={{ width: '66px', height: '66px', borderRadius: '22px', float: 'left', margin: '5px' }} /></NavLink>
          <p className='text-light fs-1 m-2 p-2' style={{ fontFamily: 'fantasy', float: 'left' }}>ClockIT</p>
        </div>
      </div>

      <div className="py-5 my-5" style={{ backgroundColor: "blanchedalmond" }}>
        <form onSubmit={handleSubmit} className='border border-dark col-md-6 bg-primary-subtle p-3' style={{ marginLeft: "570px" }}>
          <legend className='text-dark fs-2 fw-bold' style={{ textShadow: '2px 2px purple' }}>REGISTER</legend>

          <div class="form-floating mb-3 col-md-8">
            <input type="text" value={id} onChange={(e) => setid(e.target.value)} class="form-control" />
            <label for="floatingPassword">Userid</label>
          </div>

          <div class="form-floating mb-3 col-md-8">
            <input type="text" value={name} onChange={(e) => setName(e.target.value)} class="form-control" />
            <label for="floatingPassword">Name</label>
          </div>

          <div class="form-floating mb-3 col-md-8">
            <input type="text" class="form-control" value={phone} onChange={(e) => setPhone(e.target.value)} />
            <label for="floatingPassword">Phone</label>
          </div>

          <div class="form-floating mb-3 col-md-8">
            <input type="email" class="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
            <label for="floatingPassword">Email</label>
          </div>

          <div class="form-floating mb-3 col-md-8">
            <input type="password" class="form-control" value={pw} onChange={(e) => setpw(e.target.value)} />
            <label for="floatingPassword">Password</label>
          </div>

          <div class="form-floating mb-3 col-md-8">
            <input type="password" class="form-control" />
            <label for="floatingPassword">Confirm Password</label>
          </div>

          <div class="form-floating mb-3 col-md-8">
            <input type="date" class="form-control" value={dob} onChange={(e) => setDOB(e.target.value)} />
            <label for="floatingPassword">Date of Birth</label>
          </div>
          <div class=" mb-3 col-md-8">
            <label for="exampleFormControlTextarea1" class="form-label">Address</label>
            <input type='text' class="form-control" rows="3" value={address} onChange={(e) => setAddress(e.target.value)} />
          </div>

          <select class="form-select mb-3 col-md-4" value={plan} onChange={(e) => setPlan(e.target.value)} >
            <option selected>Plans opted</option>
            <option value="Regular">Regular</option>
            <option value="Preminum">Preminum</option>
            <option value="Pro+">Pro+</option>
          </select>

          <button className='btn btn-dark mx-2'>SUBMIT</button>
          <Link to='/'><button className='btn btn-success'>Back to Login</button></Link>
        </form >

      </div>
    </>
  )
}

export default Register