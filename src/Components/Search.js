import React, { useState } from 'react'
import * as i from 'react-bootstrap-icons'
import '../App.css'

function Search({ setSearch }) {
    const [input, setInput] = useState('')

    const fetchData = (value) => {
        fetch('/data/products.json')
            .then(resp => resp.json())
            .then(res => {
                const result = res.filter((prod) => {
                    return value && prod && prod.title && prod.title.toLowerCase().includes(value)
                })
                console.log(result)
                setSearch(result)
            })
    }


    const handleChange = (value) => {
        setInput(value)
        fetchData(value)
    }

    const handleClick = (e) => {
        e.preventDefault()
    }
    return (
        <>
            <div className=" container-fluid bg-body-secondary mt-2">
                <nav className="d-flex justify-content-end navbar">
                    <form className="d-flex" role="search">
                        <input className="form-control me-2" type="search" placeholder="Search Product" aria-label="Search"
                            value={input}
                            onChange={(e) => handleChange(e.target.value)} />
                        <button className="btn btn-success" style={{ width: '40px', borderRadius: '10px' }} onClick={(e) => handleClick(e)}>
                            <i.Search className='justify-content-center' />
                        </button>
                    </form>
                </nav>
            </div >

        </>
    )
}

export default Search


