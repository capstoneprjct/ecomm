

import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const EmpEdit = () => {
  const { id } = useParams();
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [designation, setDesignation] = useState("");
  const [exp, setExp] = useState("");
  const [validation, valChange] = useState(false);

  useEffect(() => {
    fetch("http://localhost:4000/employee/" + id)
      .then((res) => {
        return res.json();
      }).then((resp) => {
        console.log(resp)
        setName(resp.name);
        setDesignation(resp.designation);
        setLocation(resp.location);
        setExp(resp.exp);
      }).catch((err) => {
        console.log(err.message);
      })
  }, []);


  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    const empdata = { id, name, location, designation, exp };

    fetch("http://localhost:4000/employee/" + id, {
      method: "PUT",
      headers: { "content-type": "application/json" },
      body: JSON.stringify(empdata)
    }).then((res) => {
      alert('Saved successfully.')
      navigate('/Employee');
    }).catch((err) => {
      console.log(err.message)
    })
  }

  return (
    <>

      <form className='col border border-secondary border-4 ps-1 m-3 ps-5 pt-3' onSubmit={(e) => handleSubmit(e)}>
        <label for="name" class="form-label">NAME</label><br />
        <input required placeholder="Name" value={name} onMouseDown={() => valChange(true)} onChange={(e) => setName(e.target.value)} /><br />
        {name.length == 0 && name.length <= 25 && validation && <span className='text-danger'>Enter a valid Name (less than 20 chars)</span>}<br />

        <label for="designation" class="form-label">DESIGNATION</label><br />
        <input required placeholder="Designation" value={designation} onMouseDown={() => valChange(true)} onChange={(e) => setDesignation(e.target.value)} /><br />
        {designation.length == 0 && name.length <= 15 && validation && <span className='text-danger'>Enter a valid Designation </span>}<br />


        <label for="location" class="form-label">LOCATION</label><br />
        <input required placeholder="Location" value={location} onMouseDown={() => valChange(true)} onChange={(e) => setLocation(e.target.value)} /><br />
        {location.length == 0 && name.length <= 25 && validation && <span className='text-danger'>Enter a valid Location</span>}<br />


        <label for="exp" class="form-label">EXPERIENCE</label><br />
        <input required placeholder="Experience" value={exp} onMouseDown={() => valChange(true)} onChange={(e) => setExp(e.target.value)} /><br />
        {exp.length == 0 && name.length <= 3 && validation && <span className='text-danger'>Enter number of years</span>}<br />

        <button className='btn btn-success m-3'>UPDATE</button>
      </form>
    </>
  );
}

export default EmpEdit;
