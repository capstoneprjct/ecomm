import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addCart, delCart } from './redux/action'
import * as i from 'react-bootstrap-icons'
import { NavLink, useNavigate } from 'react-router-dom'
const Cart = () => {
  const state = useSelector((state) => state.handleCart)
  const dispatch = useDispatch()
  // const navigate = useNavigate()
  //   useEffect(() => {
  //       let name = sessionStorage.getItem('name');
  //       if (name === '' || name === null) {
  //           navigate('/')
  //       }
  //   }, [])

  const handleAdd = (product) => {
    dispatch(addCart(product))
  }

  const handleDel = (product) => {
    dispatch(delCart(product))
  }

  var total = 0;
  const product = (product) => {
    total = total + (product.qty) * (product.price)
    return (
      <></>
    )
  }
  const emptyCart = () => {
    return (
      <>
        <div className='container fw-bold fs-2 d-flex justify-content-center'> Empty Cart </div>
        <NavLink to='/product' className='fs-3 fw-medium text-success d-flex justify-content-center m-4' style={{ textShadow: '2px 1px black', textDecoration: 'underline' }}>Add list to cart</NavLink>
      </>
    )
  }

  const cartItems = (product) => {
    return (
      <div>
        <div className=' container border border-1 border-dark mt-1' style={{ width: '700px' }}>
          <div className="row">
            <div className="col-lg-4">
              <img src={product.img} alt={product.title} height="200px" width="180px" className='m-2' />
            </div>
            <div className="col-lg-8">
              <h3>{product.title}</h3>
              <p className="lead fw-bold">
                {product.qty} X <i.CurrencyRupee />{product.price} = <i.CurrencyRupee />{(product.qty * product.price)}
              </p>
              <button className="btn btn-danger me-4" onClick={() => handleDel(product)}>
                <i.ArrowDownCircleFill style={{ width: "20px", height: "20px" }} />
              </button>
              <button className="btn btn-success me-4" onClick={() => handleAdd(product)}>
                <i.ArrowUpCircleFill style={{ width: "20px", height: "20px" }} />
              </button>
            </div>
          </div>

        </div>
      </div>
    )
  }

  const handleTotal = () => {
    return (
      <div>
        {state.map(product)}
        <div className=' fs-1 fw-bolder m-5'>Total amount = <i.CurrencyRupee />{total}</div>
      </div>
    )
  }

  const buttons = () => {
    return (
      <div className='d-grid gap-2 mx-3'>
        <NavLink to='/product' className='fs-4 btn btn-warning my-3'>Continue Shopping</NavLink>
        <button className='fs-3 btn btn-success mb-5'> Proceed with payment</button>
      </div>
    )
  }
  return (
    <div>
      {state.length === 0 && emptyCart()}
      {state.length !== 0 && state.map(cartItems)}
      {state.length !== 0 && handleTotal()}
      {state.length !== 0 && buttons()}
    </div>
  )
}
export default Cart