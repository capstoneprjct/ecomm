import React, { useEffect } from 'react'
import { NavLink, useNavigate } from 'react-router-dom';

function Home() {
    const navigate = useNavigate()
    // useEffect(() => {
    //     let name = sessionStorage.getItem('name');
    //     if (name === '' || name === null) {
    //         navigate('/')
    //     }
    // }, [])
    return (
        <>
            <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel" style={{ width: '1000px', height: '500px', margin: '60px 130px' }}>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="\images\c1.png" style={{ height: '500px', width: '1000px' }} />
                    </div>
                    <div class="carousel-item">
                        <img src="\images\c2.png" style={{ height: '500px', width: '1000px' }} />
                    </div>
                    <div class="carousel-item">
                        <img src="\images\c3.png" style={{ height: '500px', width: '1000px' }} />
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>

            <div className='row ms-5 me-5 mt bg-secondary'>
                <div class="card m-5" style={{ width: "280px", float: 'left' }}>
                    <img src="images\off1.jfif" class="card-img-top" alt="offer" />
                    <div class="card-body">
                        <p class="card-text ps-3"><b> CHRISTMAS SALE IS ON!!!!</b></p>
                    </div>
                </div>

                <div class="card m-5" style={{ width: "280px", float: 'left' }}>
                    <img src="images\off2.jfif" class="card-img-top mt-4" alt="offer" style={{ height: '70px' }} />
                    <div class="card-body">
                        <h5 class="card-title px-5"><strong>"Fossil"</strong></h5>
                        <p class="card-text">Daily deals on best brands!!!!</p>
                        <a href="/products" class="btn btn-primary ms-5">Explore Products</a>
                    </div>
                </div>

                <div class="card m-5" style={{ width: "280px", float: 'left' }}>
                    <img src="images\off3.jfif" class="card-img-top my-2" alt="offer" />
                    <div class="card-body">
                        <p class="card-text ps-4">Offers on renowned looks..</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Home