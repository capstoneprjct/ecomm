import React from 'react'
import * as i from 'react-bootstrap-icons'

function Footer() {
    return (
        <>
            <div className='container-fluid d-inline-flex bg-dark-subtle mt-3 fs-5' style={{justifyContent:'center'}}>
                <pre>
                    <i.CCircleFill className='mx-1' />2023. Follow us on
                    <div>
                        <i.Instagram className='mx-1' />
                        <i.Facebook className='mx-1' />
                        <i.Twitter className='mx-1' />
                        <i.Pinterest className='mx-1' />
                    </div>
                </pre>
            </div>
        </>
    )
}

export default Footer