import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { Link, Outlet, useNavigate } from 'react-router-dom'
import { addCart } from './redux/action'
import * as i from 'react-bootstrap-icons'
import Search, { SearchResults } from './Search'



function Product() {
    const [data, setData] = useState([])
    const [search, setSearch] = useState([])
    const navigate = useNavigate()

    useEffect(() => {
        // let name = sessionStorage.getItem('name');
        // if (name === '' || name === null) {
        //     navigate('/')
        // }
        axios.get('/data/products.json')
            .then(resp => setData(resp.data))
            .catch(error => console.log("Error in fetching the data"))
    }, [])

    const dispatch = useDispatch();
    const addProduct = (product) => {
        dispatch(addCart(product));
    }


    return (
        <>
            <Search setSearch={setSearch} />
            <div className='row'>
                {search.map(prod =>
                    <div class="card" key={prod.id} style={{ width: "350px", height: '560px', marginLeft: '60px', marginTop: '30px', alignItems: "center", float: 'left' }}>
                        <div class="card-header fs-5 fw-medium bg-info-subtle" style={{ height: '96px', width: "350px" }}>{prod.title}</div>
                        <img src={prod.img} class="card-img-top my-3" alt={prod.id} style={{ width: "200px", height: "200px" }}></img>
                        <div class="card-body bg-secondary-subtle">
                            <h5 class="card-title text-success"> Price:<i.CurrencyRupee />{prod.price}/-</h5>
                            <p class="card-text">{prod.des}</p>
                            <nav>
                                <Link to={prod.id} class="btn btn-warning mx-3">More Info</Link>
                                <Link to='/cart' class="btn btn-success mx-1" onClick={() => addProduct(prod)}>Add to cart</Link>
                            </nav>
                        </div>
                    </div>
                )}
            </div>
            <div className='fs-2 fw-bold d-flex justify-content-center'>ALL PRODUCTS</div>
            {data.map(prod =>
                <div class="card" key={prod.id} style={{ width: "350px", height: '560px', marginLeft: '60px', marginTop: '30px', alignItems: "center", float: 'left' }}>
                    <div class="card-header fs-5 fw-medium bg-info-subtle" style={{ height: '96px', width: "350px" }}>{prod.title}</div>
                    <img src={prod.img} class="card-img-top my-3" alt={prod.id} style={{ width: "200px", height: "200px" }}></img>
                    <div class="card-body bg-secondary-subtle">
                        <h5 class="card-title text-success"> Price:<i.CurrencyRupee />{prod.price}/-</h5>
                        <p class="card-text">{prod.des}</p>
                        <nav>
                            <Link to={prod.id} class="btn btn-warning mx-3">More Info</Link>
                            <button class="btn btn-success mx-1" onClick={() => addProduct(prod)}>Add to cart</button>
                        </nav>
                    </div>
                </div>
            )}
            <Outlet />
        </>
    )
}

export default Product