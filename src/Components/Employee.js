import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

function Employee() {
  const url = `http://localhost:4000/employee`
  const [name, setName] = useState("")
  const [designation, setDesignation] = useState("")
  const [location, setLocation] = useState("")
  const [exp, setExp] = useState("")
  const [validation, valChange] = useState(false)
  const [data, setData] = useState('')
  const navigate = useNavigate()

  useEffect(() => {

    // let name = sessionStorage.getItem('name');
    // if (name === '' || name === null) {
    //   navigate('/')
    // }

    fetch(url)
      .then((resp) => resp.json())
      .then((resp) => setData(resp))
      .catch((err) => console.log(err.message))
  }, [])

  // const { register, handleSubmit, formState: { errors }, setValue } = useForm();
  // const handlePost = (e) => {
  //   e.preventDefault()
  //   axios.post(url, { name, designation, location, exp })
  // }

  const handleSubmit = (e) => {
    e.preventDefault()
    const empData = { name, designation, location, exp }
    // axios.post(url, { name, designation, location, exp })
    fetch(url, {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify(empData)
    }).then((resp) => {
      console.log(resp)
      alert('Saved successfully!!!')
      window.location.reload()
    })
      .catch((err) => console.log(err.message))
  }

  const editForm = (e, id) => {
    // e.preventDefault()
    navigate("/employee/" + id)
  }

  const deleteForm = (e, id) => {
    e.preventDefault()
    if (window.confirm(`Delete ID=${id} data?`)) {
      fetch('http://localhost:4000/employee/' + id, {
        method: "DELETE"
      }).then((res) => {
        alert("Deleted Successfully")
        window.location.reload()
      }).catch((err) => console.log(err.message))
    }

  }

  return (
    <>

      <div className=' mx-2'>
        <div className='text-success fs-2 fw-bold d-flex justify-content-center my-4'>EMPLOYEE INFO</div>
        <div className='row'>

          <form className='col border border-secondary border-4 ps-1 m-3 ps-5 pt-3' onSubmit={handleSubmit}>
            <label for="name" class="form-label">NAME</label><br />
            <input required placeholder="Name" value={name} onMouseDown={() => valChange(true)} onChange={(e) => setName(e.target.value)} /><br />
            {name.length == 0 && name.length <= 25 && validation && <span className='text-danger'>Enter a valid Name (less than 20 chars)</span>}<br />

            <label for="designation" class="form-label">DESIGNATION</label><br />
            <input required placeholder="Designation" value={designation} onMouseDown={() => valChange(true)} onChange={(e) => setDesignation(e.target.value)} /><br />
            {designation.length == 0 && name.length <= 15 && validation && <span className='text-danger'>Enter a valid Designation </span>}<br />


            <label for="location" class="form-label">LOCATION</label><br />
            <input required placeholder="Location" value={location} onMouseDown={() => valChange(true)} onChange={(e) => setLocation(e.target.value)} /><br />
            {location.length == 0 && name.length <= 25 && validation && <span className='text-danger'>Enter a valid Location</span>}<br />


            <label for="exp" class="form-label">EXPERIENCE</label><br />
            <input required placeholder="Experience" value={exp} onMouseDown={() => valChange(true)} onChange={(e) => setExp(e.target.value)} /><br />
            {exp.length == 0 && name.length <= 3 && validation && <span className='text-danger'>Enter number of years</span>}<br />

            <button className='btn btn-success m-3'>SUBMIT</button>
          </form>

          <div className='col border border-secondary border-4 mx-1'>
            <table className='table border-info mt-4' style={{ width: '780px' }}>
              <thead className='table border-2 border-info text-secondary fs-4'>
                <th>ID</th>
                <th>NAME</th>
                <th>DESIGNATION</th>
                <th>LOCATION</th>
                <th>EXPERIENCE</th>
                <th>ACTION</th>
              </thead>
              <tbody>
                {data && data.map(emp => <tr key={emp.id}>
                  <td>{emp.id}</td>
                  <td>{emp.name}</td>
                  <td>{emp.designation}</td>
                  <td>{emp.location}</td>
                  <td>{emp.exp}</td>
                  <td>
                    <a className='btn btn-warning' onClick={(e) => editForm(e, emp.id)} >Edit</a>
                    <a className='btn btn-danger ms-1' onClick={(e) => deleteForm(e, emp.id)}>Delete</a>
                  </td>
                </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  )
}
//   const {register, handleSubmit, formState: {errors}, setValue } = useForm();

//
//   const [apiData, setApiData] = useState([])
//   const [editData, setEditData] = useState(false)
//   const [id, setId] = useState(0)

//   const onFormSubmit = (formData) => {
//     axios.post(url, formData)
//       .then(resp => {
//         getData()
//       })
//   }

//   const editForm = (data) => {
//     setId(data.id)
//     console.log(`${id} is set for edit`)
//     setValue('id', id)
//     setValue('name', data.name)
//     setValue('designation', data.designation)
//     setValue('location', data.location)
//     setValue('exp', data.exp)
//     setEditData(true)
//   }

//   const deleteForm = (id) => {
//     console.log(`${id} is set to delete`)
//     axios.delete(`${url}/${id}`)
//       .then(resp => {
//         alert(`${id} deleted!!!`)
//         getData()
//       })
//   }

//   const getData = () => {
//     axios.get(url)
//       .then(resp => {
//         console.log(resp.data)
//         setApiData(resp.data)
//       })
//   }

//   useEffect(() => {
//     getData()
//   }, [])
//   return (
//     <>
//       <div className=' col-lg-12 text-success fs-2 fw-bold d-flex justify-content-center'>EMPLOYEE INFO</div>
//       <div className='container-fluid row'>
//         <form className='col-lg-3 border border-secondary border-4' onSubmit={handleSubmit(onFormSubmit)}>
//           {/* <label for="id" class="form-label">ID</label><br/>
//           <input {...register('id', { required: true, pattern: /^[a-zA-Z0-9\s]{6}$/ })} /><br/>
//           {errors?.id?.type === "required" && <p style={{ color: 'red' }}>* ID is required</p>}
//           {errors?.id?.type === "pattern" && <p style={{ color: 'red' }}>* ID should be valid & of 6 Alpha-characters</p>} */}

//           <label for="name" class="form-label">NAME</label><br/>
//           <input value={name}{...register('Name', { required: true, pattern: /^[a-zA-Z\s]{4,15}$/ })} />
//           {errors?.Name?.type === "required" && <p style={{ color: 'red' }}>* Name is required</p>}
//           {errors?.Name?.type === "pattern" && <p style={{ color: 'red' }}>* Name should be valid & between 4 to 15 chars</p>}

//           <label for="designation" class="form-label">DESIGNATION</label>
//           <input {...register('des', { required: true })} /><br/>
//           {errors?.des?.type === "required" && <p style={{ color: 'red' }}>* Designation is required</p>}

//           <label for="location" class="form-label">LOCATION</label><br/>
//           <input {...register('loc', { required: true, pattern: /^[a-zA-Z\s]{4,15}$/ })} />
//           {errors?.loc?.type === "required" && <p style={{ color: 'red' }}>* Location is required</p>}<br/>
//           {errors?.Name?.type === "pattern" && <p style={{ color: 'red' }}>* Location should contain only characters</p>}

//           <label for="exp" class="form-label">EXPERIENCE</label><br/>
//           <input {...register('exp', { required: true, pattern: /^[0-9]$/ })} />
//           {errors?.loc?.type === "required" && <p style={{ color: 'red' }}>* Experience in years required</p>}
//           {errors?.Name?.type === "pattern" && <p style={{ color: 'red' }}>* Experience in years</p>}
//           <button className='btn btn-success m-3'>SUBMIT</button>
//         </form>
//         
//             



export default Employee