import { Route, Routes } from "react-router-dom";
import React, { Suspense } from "react";
import Header from "./Components/Header";
import Home from "./Components/Home";
import Footer from "./Components/Footer";
import AboutUs from "./Components/AboutUs";
import Cart from "./Components/Cart";
// import Product from "./Components/Product";
import Register from "./Components/Register";
import Login from "./Components/Login";
import ProductsDetails from "./Components/ProductsDetails";
import NotFound from "./Components/NotFound";
import Employee from "./Components/Employee";
import EmpEdit from "./Components/crud/EmpEdit";

const LazyLoading = React.lazy(() => import("./Components/Product"))

function App() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/about" element={<AboutUs />} />
        <Route path="/register" element={<Register />} />
        <Route path="/" element={<Login />} />
        <Route path="/product" element={
          <Suspense fallback={<div> Fetching...</div>}>
            <LazyLoading />
            {/* <Route path=":id" element={<ProductsDetails />} /> */}
          </Suspense>}>

        </Route>
        <Route path="/product/:id" element={<ProductsDetails />} />
        <Route path="/employee" element={<Employee />} />
        <Route path="/employee/:id" element={<EmpEdit />} />
        <Route path="/cart" element={<Cart />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
      <Footer />
    </>

  );
}

export default App;
